package ba.etf.bp.controller;

import ba.etf.bp.dto.LoginDataRequest;
import ba.etf.bp.dto.LoginDataResponse;
import ba.etf.bp.model.User;
import ba.etf.bp.service.AuthService;
import ba.etf.bp.service.JwtService;
import org.hibernate.service.spi.ServiceException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin
@RequestMapping("/auth")
public class AuthController {

    @Autowired
    private AuthService _authService;

    @RequestMapping(path="", method = RequestMethod.POST)
    public @ResponseBody ResponseEntity<LoginDataResponse> login(@RequestBody LoginDataRequest request) {
        try {
            User user = _authService.checkLoginData(request.getEmail(), request.getPassword());

            if (user != null) {
                user.setPassword(null);
                String token = JwtService.issueToken(true,true);

                return ResponseEntity
                        .status(HttpStatus.OK)
                        .body(new LoginDataResponse(user, token));
            }
            else {
                throw new ServiceException("");
            }

        }
        catch (ServiceException e) {
            return ResponseEntity
                    .status(HttpStatus.BAD_REQUEST)
                    .body(new LoginDataResponse("Incorrect username or password!"));
        }
    }
}
