package ba.etf.bp.controller;

import ba.etf.bp.model.Document;
import ba.etf.bp.service.DocumentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping("/file")
public class FileUploadController {
    @Autowired
    private DocumentService documentService;

    @RequestMapping(value = "/upload", method = RequestMethod.POST)
    public void singleFileUpload(@RequestParam("file") MultipartFile file) {
        try {
            Document document = new Document(file.getOriginalFilename(), file.getBytes());
            documentService.saveDocument(document);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @RequestMapping(value = "/getAll")
    public List<Document> getAllFiles() {
        return documentService.getAllDocuments();
    }
}
