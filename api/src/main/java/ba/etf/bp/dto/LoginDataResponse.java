package ba.etf.bp.dto;

import ba.etf.bp.model.User;

public class LoginDataResponse {

    private User user;
    private String errorMessage;
    private String token;

    public LoginDataResponse() {
    }

    public LoginDataResponse(String errorMessage) {
        this.setErrorMessage(errorMessage);
    }

    public LoginDataResponse(User korisnik, String token) {
        this.setUser(korisnik);
        this.setToken(token);
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
