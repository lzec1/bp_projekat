package ba.etf.bp.model;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Arrays;

@Entity
@SequenceGenerator(name="DOCUMENT_SEQ", allocationSize=1, sequenceName = "DOCUMENT_SEQ")
public class Document {
    @Id
    @GeneratedValue(strategy= GenerationType.SEQUENCE, generator = "DOCUMENT_SEQ")
    private Integer id;

    @NotNull
    private String name;

    private byte[] document;

    public Document() {
    }

    public Document(String name, byte[] document) {
        this.name = name;
        this.document = document;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public byte[] getDocument() {
        return document;
    }

    public void setDocument(byte[] document) {
        this.document = document;
    }

    @Override
    public String toString() {
        return "Document{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", document=" + Arrays.toString(document) +
                '}';
    }
}
