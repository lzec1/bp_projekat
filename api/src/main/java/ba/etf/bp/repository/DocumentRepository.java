package ba.etf.bp.repository;

import ba.etf.bp.model.Document;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DocumentRepository extends JpaRepository<Document, Integer> {
    @Query("select d.name from Document d" )
    List<String> getAllDocumentsNames();
}
