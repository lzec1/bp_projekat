package ba.etf.bp.repository;

import ba.etf.bp.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<User, Integer> {
    User findByEmail(String email);
    User findByNameAndSurname(String name, String surname);
    User findByEmailAndPassword(String email, String password);
}
