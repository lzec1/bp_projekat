package ba.etf.bp.service;

import ba.etf.bp.model.Document;
import ba.etf.bp.repository.DocumentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DocumentService {
    @Autowired
    private DocumentRepository documentRepository;

    public List<String> getAllDocumentsNames() {
        return documentRepository.getAllDocumentsNames();
    }

    public List<Document> getAllDocuments() {
        return documentRepository.findAll();
    }

    public void saveDocument(Document document) {
        documentRepository.save(document);
    }
}



