import Ember from 'ember';

export default Ember.Controller.extend({
  sortProperties: ['timestamp'],
  sortAscending: false,
  actions: {
    onNext: function() {
      var newFile = this.store.createRecord('file', {
        title: this.get('title'),
        body: this.get('body'),
        timestamp: new Date().getTime()
      });
      newFile.save();
      this.transitionToRoute('home');
    },

    }
});