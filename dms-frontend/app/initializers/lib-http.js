export function initialize(application) {
  application.inject('route', 'lib-http', 'service:lib-http');
  application.inject('controller', 'lib-http', 'service:lib-http');
  application.inject('component', 'lib-http', 'service:lib-http');
  application.inject('route', 'base-http-service', 'service:base-http-service');
  application.inject('controller', 'base-http-service', 'service:base-http-service');
  application.inject('component', 'base-http-service', 'service:base-http-service');
}

export default {
  name: 'base-http-service',
  name2: 'lib-http',
  initialize
};
