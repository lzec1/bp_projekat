import BaseModel from './base-model';

var _modelProperties = ['id', 'name', 'surname', 'email', 'password'];

export default BaseModel.extend({
	modelProperties: _modelProperties,
});
