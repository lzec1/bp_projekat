import Ember from 'ember';
import config from './config/environment';

const Router = Ember.Router.extend({
  location: config.locationType,
  rootURL: config.rootURL
});

Router.map(function() {
  this.route('upload');
  this.route('create');
  this.route('login');
  this.route('register');
  this.route('library');
  this.route('profile');
  this.route('home');
  this.route('users');
  this.route('files');
  this.route('edit', { path: '/:id/edit' });
  this.route('preview');
});

export default Router;