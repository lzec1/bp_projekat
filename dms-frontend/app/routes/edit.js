import Ember from 'ember';

export default Ember.Route.extend({
  model: function (transition) {
    return this.get('store').findRecord('file', transition.id);
  },

  actions: {

  }
});
