import Ember from 'ember';
//import { extension } from '../utils/extension';


const {
  inject: {
    service
  }
} = Ember;

export default Ember.Route.extend({

  mockfiles: service('file-service'),

  model() {
    return Ember.RSVP.hash({
      posts: this.store.findAll('file'),
      uploads: this.get('mockfiles').getAllfiles(),
    });
  },

  actions: {
    onDelete: function (post) {
      post.destroyRecord();
    },
    selectItem: function (post) {
      var names = post.name.split(".");
      var length= names.length;
      var extension =names[length-1];
      var string = '';
      if(extension == 'jpeg' || extension == 'png' || extension == 'gif' || extension == 'jpg') {
        string = "data:image/png;base64,"+post.document;
      }
      else if(extension == 'pdf'){
       string = "data:application/pdf;base64,"+post.document;
      }
      else if(extension == 'docx' || extension == 'doc' ){
        // download : string = "data:application/msword;base64,"+post.document;
        string = "data:application/vnd.openxmlformats-officedocument.wordprocessingml.document;base64,"+post.document;

      }
        var iframe = "<iframe width='100%' height='100%' src='" + string + "'></iframe>"
        var x = window.open();
        x.document.open();
        x.document.write(iframe);
        x.document.close();
    }
  }
});
