import Ember from 'ember';

export default Ember.Route.extend({
  // ---------------------------------------------------------------------------
  // Ember Properties
  // ---------------------------------------------------------------------------
  beforeModel: function () {
    this.transitionTo('home');
  }
});