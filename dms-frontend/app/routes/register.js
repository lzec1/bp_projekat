import Ember from 'ember';

const {
  inject: {
    service
  }
} = Ember;

export default Ember.Route.extend({

  _userService: service('user-service'),

  model: function () {
    //returns an empty user model
    return this.get('_userService').createUser();
  },

  actions: {
    onNext: function () {
      //console.log(this.controller.get('model'));
      this.get('_userService').registerUser(this.controller.get('model'))
      .then(()=> this.transitionTo('home'));
    },
    onCancel: function () {
      this.transitionTo('home');
    },

  }
});
