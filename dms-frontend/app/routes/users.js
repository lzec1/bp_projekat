import Ember from 'ember';

const {
  inject: {
    service
  }
} = Ember;

export default Ember.Route.extend({

  userService: service('user-service'),

  // ---------------------------------------------------------------------------
  // Ember Properties
  // ---------------------------------------------------------------------------

   model: function () {
    return this.get('userService').getAllUsers();
  },


});
