import Ember from 'ember';
import BaseHttpService from './base-http-service';

export default BaseHttpService.extend({

  currentUser: null,

  createUser() {
    let newUser = Ember.Object.create({
      name: '',
      surname: '',
      email: '',
      password: '',
      role:'',
    });
    this.set('currentUser', newUser);
    return this.get('currentUser');
  },

  getAllUsers() {
    return this.ajax('GET', '/user/all');
  },

  getUserById(id) {
    return this.ajax('GET', `/user/${id}`);
  },

  getUserByEmail(email) {
    return this.ajax('GET', '/user/find?email=' + email);
  },

  registerUser: function (user) {
    return this.ajax('POST', '/user/new', user);
  },

});
