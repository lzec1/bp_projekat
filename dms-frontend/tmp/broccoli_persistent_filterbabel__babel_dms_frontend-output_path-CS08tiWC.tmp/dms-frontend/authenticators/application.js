define('dms-frontend/authenticators/application', ['exports', 'ember-simple-auth/authenticators/base'], function (exports, _base) {
    'use strict';

    Object.defineProperty(exports, "__esModule", {
        value: true
    });
    var Promise = Ember.RSVP.Promise;
    exports.default = _base.default.extend({
        libHttp: Ember.inject.service('lib-http'),

        restore: function restore(data) {
            return new Promise(function (resolve, reject) {
                if (!Ember.isEmpty(data.token)) {
                    resolve(data);
                } else {
                    reject();
                }
            });
        },
        authenticate: function authenticate(credentials, callback) {
            return this.get('libHttp').post('auth', credentials, function (resp) {
                if (callback) {
                    callback(resp);
                }

                return resp;
            });
        },
        invalidate: function invalidate(data) {
            return Promise.resolve(data);
        }
    });
});