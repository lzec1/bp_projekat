define('dms-frontend/controllers/login', ['exports'], function (exports) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.default = Ember.Controller.extend({
    session: Ember.inject.service(),

    model: {},
    errorMessage: '',

    actions: {
      login: function login() {
        var _this = this;

        this.get('session').authenticate('authenticator:application', this.model, function (data) {
          console.log(data);
          Ember.set(_this, 'errorMessage', '');
          Ember.set(_this, 'model', {});
          alert("Welcome");
          window.location.reload();
        }).catch(function (reason) {
          Ember.set(_this, 'errorMessage', JSON.parse(reason.responseText).errorMessage);
          _this.set('authenticationError', _this.errorMessage || reason);
        });
      },

      onCancel: function onCancel() {
        this.transitionToRoute('/');
      }
    }
  });
});