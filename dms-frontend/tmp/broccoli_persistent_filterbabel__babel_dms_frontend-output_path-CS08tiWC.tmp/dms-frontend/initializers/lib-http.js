define('dms-frontend/initializers/lib-http', ['exports'], function (exports) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.initialize = initialize;
  function initialize(application) {
    application.inject('route', 'lib-http', 'service:lib-http');
    application.inject('controller', 'lib-http', 'service:lib-http');
    application.inject('component', 'lib-http', 'service:lib-http');
    application.inject('route', 'base-http-service', 'service:base-http-service');
    application.inject('controller', 'base-http-service', 'service:base-http-service');
    application.inject('component', 'base-http-service', 'service:base-http-service');
  }

  exports.default = {
    name: 'base-http-service',
    name2: 'lib-http',
    initialize: initialize
  };
});