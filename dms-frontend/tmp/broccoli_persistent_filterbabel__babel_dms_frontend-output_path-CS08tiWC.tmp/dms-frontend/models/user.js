define('dms-frontend/models/user', ['exports', 'dms-frontend/models/base-model'], function (exports, _baseModel) {
	'use strict';

	Object.defineProperty(exports, "__esModule", {
		value: true
	});


	var _modelProperties = ['id', 'name', 'surname', 'email', 'password'];

	exports.default = _baseModel.default.extend({
		modelProperties: _modelProperties
	});
});