define('dms-frontend/router', ['exports', 'dms-frontend/config/environment'], function (exports, _environment) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });


  var Router = Ember.Router.extend({
    location: _environment.default.locationType,
    rootURL: _environment.default.rootURL
  });

  Router.map(function () {
    this.route('upload');
    this.route('create');
    this.route('login');
    this.route('register');
    this.route('library');
    this.route('profile');
    this.route('home');
    this.route('users');
  });

  exports.default = Router;
});