define('dms-frontend/routes/index', ['exports'], function (exports) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.default = Ember.Route.extend({
    // ---------------------------------------------------------------------------
    // Ember Properties
    // ---------------------------------------------------------------------------
    beforeModel: function beforeModel() {
      this.transitionTo('home');
    }
  });
});