define('dms-frontend/routes/login', ['exports'], function (exports) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.default = Ember.Route.extend({
    session: Ember.inject.service(),

    beforeModel: function beforeModel(transition) {
      //ovo je ok
      if (this.get('session.isAuthenticated')) {
        var previousTransition = this.get('previousTransition');
        if (previousTransition) {
          this.set('previousTransition', null);
          previousTransition.retry();
        } else {
          // Default back to homepage
          this.transitionTo('home');
        }
      }
      //ako vec postoji sesija, ne moze se logovati
    }
  });
});