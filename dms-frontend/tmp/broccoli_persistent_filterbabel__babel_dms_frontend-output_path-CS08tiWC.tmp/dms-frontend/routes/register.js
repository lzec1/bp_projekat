define('dms-frontend/routes/register', ['exports'], function (exports) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  var service = Ember.inject.service;
  exports.default = Ember.Route.extend({

    _userService: service('user-service'),

    model: function model() {
      //returns an empty user model
      return this.get('_userService').createUser();
    },

    actions: {
      onNext: function onNext() {
        var _this = this;

        //console.log(this.controller.get('model'));
        this.get('_userService').registerUser(this.controller.get('model')).then(function () {
          return _this.transitionTo('home');
        });
      },
      onCancel: function onCancel() {
        this.transitionTo('home');
      }

    }
  });
});