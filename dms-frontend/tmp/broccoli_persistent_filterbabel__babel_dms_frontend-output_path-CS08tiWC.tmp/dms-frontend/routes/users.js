define('dms-frontend/routes/users', ['exports'], function (exports) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  var service = Ember.inject.service;
  exports.default = Ember.Route.extend({

    userService: service('user-service'),

    // ---------------------------------------------------------------------------
    // Ember Properties
    // ---------------------------------------------------------------------------

    model: function model() {
      return this.get('userService').getAllUsers();
    }

  });
});