define('dms-frontend/services/lib-http', ['exports'], function (exports) {
    'use strict';

    Object.defineProperty(exports, "__esModule", {
        value: true
    });
    var ajax = Ember.$.ajax,
        run = Ember.run;
    var RSVP = Ember.RSVP;
    exports.default = Ember.Service.extend({
        _innerCreateMethod: function _innerCreateMethod(route, data, httpRequest, happyPath, unhappyPath) {
            var requestOptions = {
                url: 'http://localhost:8080//' + route,
                contentType: 'application/json; charset=utf-8',
                type: httpRequest,
                dataType: 'json'
            };

            if (data) {
                requestOptions.data = JSON.stringify(data);
            }

            return new RSVP.Promise(function (resolve, reject) {
                ajax(requestOptions).then(function (response) {
                    run(function () {
                        if (happyPath) {
                            return resolve(happyPath(response));
                        } else {
                            return resolve();
                        }
                    });
                }, function (error) {
                    run(function () {
                        if (unhappyPath) {
                            return unhappyPath(error);
                        }

                        return reject(error);
                    });
                });
            });
        },
        get: function get(route, data, happyPath, unhappyPath) {
            return this._innerCreateMethod(route, null, 'GET', happyPath, unhappyPath);
        },
        post: function post(route, data, happyPath, unhappyPath) {
            return this._innerCreateMethod(route, data, 'POST', happyPath, unhappyPath);
        }
    });
});