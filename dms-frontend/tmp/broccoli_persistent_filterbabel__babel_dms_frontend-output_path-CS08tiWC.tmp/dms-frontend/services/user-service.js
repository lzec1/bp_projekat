define('dms-frontend/services/user-service', ['exports', 'dms-frontend/services/base-http-service'], function (exports, _baseHttpService) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.default = _baseHttpService.default.extend({

    currentUser: null,

    createUser: function createUser() {
      var newUser = Ember.Object.create({
        name: '',
        surname: '',
        email: '',
        password: ''
      });
      this.set('currentUser', newUser);
      return this.get('currentUser');
    },
    getAllUsers: function getAllUsers() {
      return this.ajax('GET', '/user/all');
    },
    getUserById: function getUserById(id) {
      return this.ajax('GET', '/user/' + id);
    },
    getUserByEmail: function getUserByEmail(email) {
      return this.ajax('GET', '/user/find?email=' + email);
    },


    registerUser: function registerUser(user) {
      return this.ajax('POST', '/user/new', user);
    }

  });
});