define('dms-frontend/routes/home', ['exports'], function (exports) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  var service = Ember.inject.service;
  exports.default = Ember.Route.extend({

    userService: service('user-service')

    // ---------------------------------------------------------------------------
    // Ember Properties
    // ---------------------------------------------------------------------------


  });
});