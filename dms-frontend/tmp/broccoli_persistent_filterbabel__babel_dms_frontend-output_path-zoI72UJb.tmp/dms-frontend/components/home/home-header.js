define('dms-frontend/components/home/home-header', ['exports'], function (exports) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.default = Ember.Component.extend({
    // ---------------------------------------------------------------------------
    // Ember Properties
    // ---------------------------------------------------------------------------
    classNames: ['home-header']
  });
});