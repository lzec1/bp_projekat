define('dms-frontend/services/user-service', ['exports', 'dms-frontend/services/base-http-service'], function (exports, _baseHttpService) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.default = _baseHttpService.default.extend({

    currentUser: null,

    createUser: function createUser() {
      var newUser = Ember.Object.create({
        first_name: '',
        last_name: '',
        email: '',
        password: ''
      });
      this.set('currentUser', newUser);
      return this.get('currentUser');
    },
    getAllUsers: function getAllUsers() {
      return this.ajax('GET', '');
    },
    getUserById: function getUserById() {
      return this.ajax('GET', '');
    },
    getUserByEmail: function getUserByEmail() {
      return this.ajax('GET', '');
    },


    registerUser: function registerUser(user) {
      return this.ajax('POST', '', user);
    }

  });
});