define('dms-frontend/tests/app.lint-test', [], function () {
  'use strict';

  QUnit.module('ESLint | app');

  QUnit.test('app.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'app.js should pass ESLint\n\n');
  });

  QUnit.test('components/home/home-header.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'components/home/home-header.js should pass ESLint\n\n');
  });

  QUnit.test('controllers/register.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'controllers/register.js should pass ESLint\n\n');
  });

  QUnit.test('resolver.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'resolver.js should pass ESLint\n\n');
  });

  QUnit.test('router.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'router.js should pass ESLint\n\n');
  });

  QUnit.test('routes/create.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'routes/create.js should pass ESLint\n\n');
  });

  QUnit.test('routes/home.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'routes/home.js should pass ESLint\n\n');
  });

  QUnit.test('routes/index.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'routes/index.js should pass ESLint\n\n');
  });

  QUnit.test('routes/library.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'routes/library.js should pass ESLint\n\n');
  });

  QUnit.test('routes/login.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'routes/login.js should pass ESLint\n\n');
  });

  QUnit.test('routes/profile.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'routes/profile.js should pass ESLint\n\n');
  });

  QUnit.test('routes/register.js', function (assert) {
    assert.expect(1);
    assert.ok(false, 'routes/register.js should pass ESLint\n\n20:7 - Unexpected console statement. (no-console)');
  });

  QUnit.test('routes/upload.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'routes/upload.js should pass ESLint\n\n');
  });

  QUnit.test('services/base-http-service.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'services/base-http-service.js should pass ESLint\n\n');
  });

  QUnit.test('services/user-service.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'services/user-service.js should pass ESLint\n\n');
  });
});