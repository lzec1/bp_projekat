QUnit.module('ESLint | app');

QUnit.test('app.js', function(assert) {
  assert.expect(1);
  assert.ok(true, 'app.js should pass ESLint\n\n');
});

QUnit.test('authenticators/application.js', function(assert) {
  assert.expect(1);
  assert.ok(true, 'authenticators/application.js should pass ESLint\n\n');
});

QUnit.test('components/home/home-header.js', function(assert) {
  assert.expect(1);
  assert.ok(true, 'components/home/home-header.js should pass ESLint\n\n');
});

QUnit.test('controllers/application.js', function(assert) {
  assert.expect(1);
  assert.ok(true, 'controllers/application.js should pass ESLint\n\n');
});

QUnit.test('controllers/login.js', function(assert) {
  assert.expect(1);
  assert.ok(false, 'controllers/login.js should pass ESLint\n\n12:13 - Unexpected console statement. (no-console)');
});

QUnit.test('controllers/register.js', function(assert) {
  assert.expect(1);
  assert.ok(true, 'controllers/register.js should pass ESLint\n\n');
});

QUnit.test('initializers/lib-http.js', function(assert) {
  assert.expect(1);
  assert.ok(true, 'initializers/lib-http.js should pass ESLint\n\n');
});

QUnit.test('models/base-model.js', function(assert) {
  assert.expect(1);
  assert.ok(true, 'models/base-model.js should pass ESLint\n\n');
});

QUnit.test('models/user.js', function(assert) {
  assert.expect(1);
  assert.ok(true, 'models/user.js should pass ESLint\n\n');
});

QUnit.test('resolver.js', function(assert) {
  assert.expect(1);
  assert.ok(true, 'resolver.js should pass ESLint\n\n');
});

QUnit.test('router.js', function(assert) {
  assert.expect(1);
  assert.ok(true, 'router.js should pass ESLint\n\n');
});

QUnit.test('routes/application.js', function(assert) {
  assert.expect(1);
  assert.ok(true, 'routes/application.js should pass ESLint\n\n');
});

QUnit.test('routes/create.js', function(assert) {
  assert.expect(1);
  assert.ok(true, 'routes/create.js should pass ESLint\n\n');
});

QUnit.test('routes/home.js', function(assert) {
  assert.expect(1);
  assert.ok(true, 'routes/home.js should pass ESLint\n\n');
});

QUnit.test('routes/index.js', function(assert) {
  assert.expect(1);
  assert.ok(true, 'routes/index.js should pass ESLint\n\n');
});

QUnit.test('routes/library.js', function(assert) {
  assert.expect(1);
  assert.ok(true, 'routes/library.js should pass ESLint\n\n');
});

QUnit.test('routes/login.js', function(assert) {
  assert.expect(1);
  assert.ok(false, 'routes/login.js should pass ESLint\n\n6:13 - \'transition\' is defined but never used. (no-unused-vars)');
});

QUnit.test('routes/profile.js', function(assert) {
  assert.expect(1);
  assert.ok(true, 'routes/profile.js should pass ESLint\n\n');
});

QUnit.test('routes/register.js', function(assert) {
  assert.expect(1);
  assert.ok(true, 'routes/register.js should pass ESLint\n\n');
});

QUnit.test('routes/upload.js', function(assert) {
  assert.expect(1);
  assert.ok(true, 'routes/upload.js should pass ESLint\n\n');
});

QUnit.test('routes/users.js', function(assert) {
  assert.expect(1);
  assert.ok(true, 'routes/users.js should pass ESLint\n\n');
});

QUnit.test('services/base-http-service.js', function(assert) {
  assert.expect(1);
  assert.ok(true, 'services/base-http-service.js should pass ESLint\n\n');
});

QUnit.test('services/lib-http.js', function(assert) {
  assert.expect(1);
  assert.ok(true, 'services/lib-http.js should pass ESLint\n\n');
});

QUnit.test('services/user-service.js', function(assert) {
  assert.expect(1);
  assert.ok(true, 'services/user-service.js should pass ESLint\n\n');
});

